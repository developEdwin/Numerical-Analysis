{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 8. Find the third Taylor polynomial $P_3 (x)$ for the function $f(x) = \\sqrt{x+1}$ about $x_0 = 0.$ Approximate $\\sqrt{0.5}, \\sqrt{0.75}, \\sqrt{1.25}, \\text{ and } \\sqrt{1.5} \\text{ using } P_3 (x),$ and find the actual errors."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Even though this problem is quite the same as the last one, we have two options, to calculate the derivatives by hand (this will be the _analytical approach_), or to calculate the derivatives numerically using **scipy** given that the Taylor formula only asks for the derivatives evaluated at the point we are expanding about. \n",
    "We will do this in a _hybrid_ fashion, we will first calculate the derivatives numerically and substitute those values in the Taylor formula which we will write by hand."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import the derivative subroutine from scipy\n",
    "# Also import the square root function from the math library\n",
    "import math\n",
    "from scipy.misc import derivative"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "First derivative: 0.5000\n",
      "Second derivative: -0.2500\n",
      "Third derivative: 0.3750\n"
     ]
    }
   ],
   "source": [
    "# First off, we need to define the function we will be differentiating\n",
    "def f_1(x):\n",
    "    return math.sqrt(x + 1)\n",
    "\n",
    "# We will now use the derivative subroutine to calculate the derivatives\n",
    "# The derivative subroutine has the following syntax:\n",
    "# derivative(function, point at which the derivative is calculated, n=order of the derivative)\n",
    "# First derivative\n",
    "print('First derivative: {0:.4f}'.format(derivative(f_1, 0., dx=0.01, n=1)))\n",
    "\n",
    "# Second derivative\n",
    "print('Second derivative: {0:.4f}'.format(derivative(f_1, 0., dx=0.01, n=2, order=7)))\n",
    "\n",
    "# Third derivative\n",
    "print('Third derivative: {0:.4f}'.format(derivative(f_1, 0., dx=0.01, n=3, order=7)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Just a quick **note** here: we needed to specify the spacing using the _dx_ option in the subroutine. This was necessary because of the domain of the function, so we had to lower it from 1.0 (which is the default value) to 0.01. We also had to modify the _order_ option in the subroutine because it needs at least an order of _n + 1_, where _n_ is the actual order of the derivative given by the _n_ option. This _order_ option takes more points given that the _derivative_ subroutine uses _central differences_ (a numerical method for calculating derivatives which will be addressed later in the book), so this will actually calculate the correct derivative.\n",
    "\n",
    "Now that we have the values of the derivatives, we have the following series expansion:\n",
    "$$ f(x) = 1 + \\frac{0.5 }{1!} x - \\frac{0.25 }{2!} x^2 + \\frac{0.375 }{3!} x^3 + \\mathcal{O}(x^4),$$ which simplifies to:\n",
    "$$ f(x) = 1 + 0.5x - 0.125 x^2 + 0.0625 x^3 + \\mathcal{O}(x^4) .$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "For 0.5:\t1.2247449\t1.2265625\t0.0018176\n",
      "\n",
      "For 0.75:\t1.3228757\t1.3310547\t0.0081790\n",
      "\n",
      "For 1.25:\t1.5000000\t1.5517578\t0.0517578\n",
      "\n",
      "For 1.5:\t1.5811388\t1.6796875\t0.0985487\n",
      "\n"
     ]
    }
   ],
   "source": [
    "# We now define a function using the Taylor polynomial\n",
    "def taylor_f1(x):\n",
    "    return 1 + 0.5*x - 0.125*x**2 + 0.0625*x**3\n",
    "\n",
    "# Create an array for the values meant to be calculated with the actual function\n",
    "# and the Taylor polynomial\n",
    "values = [0.5, 0.75, 1.25, 1.5]\n",
    "\n",
    "# Iterate over all these values using both functions\n",
    "for v in values:\n",
    "    print('For {0}:\\t{1:.7f}\\t{2:.7f}\\t{3:.7f}\\n'.format(v,\n",
    "        f_1(v), taylor_f1(v), math.fabs(f_1(v) - taylor_f1(v))))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And for clarity, here is a Markdown table, with Latex typesetting:\n",
    "\n",
    "|                   x             |    0.5    |    0.75   |    1.25   |    1.5    |\n",
    "|:-------------------------------:|:---------:|:---------:|:---------:|:---------:|\n",
    "|              $P_3(x)$           | 1.2265625 | 1.3310547 | 1.5517578 | 1.6796875 |\n",
    "|             $\\sqrt{x + 1}$      | 1.2247449 | 1.3228757 | 1.5000000 | 1.5811388 |\n",
    "|             $\\epsilon$          | 0.0018176 | 0.0081790 | 0.0517578 | 0.0985487 |\n",
    "\n",
    "where $\\epsilon = \\left|\\sqrt{x+1}-P_3(x)\\right|$"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
