## Numerical Analysis, 10th edition, by Richard L. Burden, J. Douglas Faires & Annette M. Burden

The first attempt at writing all the solutions was intended to be written in the [Julia programming language](https://julialang.org/).

Back then, the latest edition of the book was the 9th edition and I promptly switched to Python 3, not because Julia was bad or
anything like that, but because I wanted to finish the book faster (which I failed miserably).

I now return to Julia and intend to fully finish the 10th edition of this book in the hopes that someone finds this useful.
The book actually makes a very good and respectable effort on writing pseudocode instead of using a particular language, but
I thought, and still think, that Julia was a great candidate (and indeed it is) for numerical analysis courses.
